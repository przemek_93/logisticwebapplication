import { Component, OnInit } from '@angular/core';
import { CourierDetailService } from 'src/app/shared/courier-detail.service';
import { CourierDetail } from 'src/app/shared/courier-detail.model';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-courier-detail-list',
  templateUrl: './courier-detail-list.component.html',
  styles: []
})
export class CourierDetailListComponent implements OnInit {

  constructor(private service: CourierDetailService,
    private toastr: ToastrService) { }

  ngOnInit() {
    this.service.refreshCouriersList();
  }

  populateForm(cd:CourierDetail) {
    this.service.formData = Object.assign({},cd);
  }

  onDelete(CourierId) {
    if(confirm('Are you sure to delete this record ?')){
      this.service.deleteCourierDetail(CourierId)
        .subscribe(res =>{
          this.service.refreshCouriersList();
          this.toastr.warning('Deleted successfully', 'Couriers Register')
        },
          err =>{
            console.log(err);
          })
    }
  }

}

