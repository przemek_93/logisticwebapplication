import { Component, OnInit } from '@angular/core';
import { CourierDetailService } from 'src/app/shared/courier-detail.service';
import { NgForm } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-courier-detail',
  templateUrl: './courier-detail.component.html',
  styles: []
})
export class CourierDetailComponent implements OnInit {

  constructor(private service:CourierDetailService,
    private toastr: ToastrService) { }

  ngOnInit() {
    this.resetCourierForm();
  }

  resetCourierForm(form?: NgForm){
    if(form!=null)
      form.resetForm();
    this.service.formData = {
      CourierId: 0,
      FirstName: '',
      LastName: '',
      PhoneNumber: ''
    }
  }

  onCourierSubmit(form:NgForm){
    if(this.service.formData.CourierId == 0)
      this.insertCourierRecord(form);
    else
      this.updateCourierRecord(form);
  }

  insertCourierRecord(form: NgForm){
    this.service.postCourierDetail().subscribe(
      res => {
        this.resetCourierForm(form);
        this.toastr.success('Submitted succesfully', 'Couriers Register');
        this.service.refreshCouriersList();
      },
      err => {
        console.log(err);
      }
    )
  }

  updateCourierRecord(form: NgForm){
    this.service.putCourierDetail().subscribe(
      res => {
        this.resetCourierForm(form);
        this.toastr.info('Submitted succesfully', 'Courier Detail Register');
        this.service.refreshCouriersList();
      },
      err => {
        console.log(err);
      }
    )
  }

}
