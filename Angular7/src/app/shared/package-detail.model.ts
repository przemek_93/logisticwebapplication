export class PackageDetail {
    PackageId :number;
    Sender :string;
    Recipient :string;
    SendingDate :string;
    Courier :string; 
}
