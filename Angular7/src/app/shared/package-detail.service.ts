import { Injectable } from '@angular/core';
import { PackageDetail } from './package-detail.model';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class PackageDetailService {
  formData:PackageDetail;
  readonly rootURL ='http://localhost:50706/api';
  list : PackageDetail[];

  constructor(private http:HttpClient) { }

  postPackageDetail(){
    return this.http.post(this.rootURL + '/PackageDetails/',this.formData);
  }

  putPackageDetail(){
    return this.http.put(this.rootURL + '/PackageDetails/' + this.formData.PackageId, this.formData);
  }

  deletePackageDetail(id){
    return this.http.delete(this.rootURL + '/PackageDetails/' + id);
  }

  refreshPackagesList(){
    this.http.get(this.rootURL + '/PackageDetails/')
    .toPromise()
    .then(res => this.list = res as PackageDetail[])
  }
}
