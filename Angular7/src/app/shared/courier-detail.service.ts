import { Injectable } from '@angular/core';
import { CourierDetail } from './courier-detail.model';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class CourierDetailService {
  formData:CourierDetail;
  readonly rootURL ='http://localhost:50706/api';
  list : CourierDetail[];

  constructor(private http:HttpClient) { }

  postCourierDetail(){
    return this.http.post(this.rootURL + '/CourierDetails/',this.formData);
  }

  putCourierDetail(){
    return this.http.put(this.rootURL + '/CourierDetails/' + this.formData.CourierId, this.formData);
  }

  deleteCourierDetail(id){
    return this.http.delete(this.rootURL + '/CourierDetails/' + id);
  }

  refreshCouriersList(){
    this.http.get(this.rootURL + '/CourierDetails/')
    .toPromise()
    .then(res => this.list = res as CourierDetail[])
  }
}
