import { Component, OnInit } from '@angular/core';
import { PackageDetailService } from 'src/app/shared/package-detail.service';
import { NgForm } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-package-detail',
  templateUrl: './package-detail.component.html',
  styles: []
})
export class PackageDetailComponent implements OnInit {

  constructor(private service:PackageDetailService,
    private toastr: ToastrService) { }

  ngOnInit() {
    this.resetPackageForm();
  }

  resetPackageForm(form?: NgForm){
    if(form!=null)
      form.resetForm();
    this.service.formData = {
      PackageId: 0,
      Sender: '',
      Recipient: '',
      SendingDate: '',
      Courier: ''
    }
  }

  onPackageSubmit(form:NgForm){
    if(this.service.formData.PackageId == 0)
      this.insertPackageRecord(form);
    else
      this.updatePackageRecord(form);
  }

  insertPackageRecord(form: NgForm){
    this.service.postPackageDetail().subscribe(
      res => {
        this.resetPackageForm(form);
        this.toastr.success('Submitted succesfully', 'Packages Register');
        this.service.refreshPackagesList();
      },
      err => {
        console.log(err);
      }
    )
  }

  updatePackageRecord(form: NgForm){
    this.service.putPackageDetail().subscribe(
      res => {
        this.resetPackageForm(form);
        this.toastr.info('Submitted succesfully', 'Package Detail Register');
        this.service.refreshPackagesList();
      },
      err => {
        console.log(err);
      }
    )
  }

}