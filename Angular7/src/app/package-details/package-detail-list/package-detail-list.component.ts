import { Component, OnInit } from '@angular/core';
import { PackageDetailService } from 'src/app/shared/package-detail.service';
import { PackageDetail } from 'src/app/shared/package-detail.model';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-package-detail-list',
  templateUrl: './package-detail-list.component.html',
  styles: []
})
export class PackageDetailListComponent implements OnInit {

  constructor(private service: PackageDetailService,
    private toastr: ToastrService) { }

  ngOnInit() {
    this.service.refreshPackagesList();
  }

  populateForm(pd:PackageDetail) {
    this.service.formData = Object.assign({},pd);
  }

  onDelete(PackageId) {
    if(confirm('Are you sure to delete this record ?')){
      this.service.deletePackageDetail(PackageId)
        .subscribe(res =>{
          this.service.refreshPackagesList();
          this.toastr.warning('Deleted successfully', 'Packages Register')
        },
          err =>{
            console.log(err);
          })
    }
  }

}

