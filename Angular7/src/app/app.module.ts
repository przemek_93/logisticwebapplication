import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
 
import { ToastrModule } from 'ngx-toastr';
import { AppComponent } from './app.component';
import { CourierDetailsComponent } from './courier-details/courier-details.component';
import { CourierDetailComponent } from './courier-details/courier-detail/courier-detail.component';
import { CourierDetailListComponent } from './courier-details/courier-detail-list/courier-detail-list.component';
import { CourierDetailService } from './shared/courier-detail.service';
import { PackageDetailsComponent } from './package-details/package-details.component';
import { PackageDetailComponent } from './package-details/package-detail/package-detail.component';
import { PackageDetailListComponent } from './package-details/package-detail-list/package-detail-list.component';
import { PackageDetailService } from './shared/package-detail.service';

@NgModule({
  declarations: [
    AppComponent,
    CourierDetailsComponent,
    CourierDetailComponent,
    CourierDetailListComponent,
    PackageDetailsComponent,
    PackageDetailComponent,
    PackageDetailListComponent,
  ],
  imports: [
    BrowserModule,
    FormsModule,

    HttpClientModule,
    BrowserAnimationsModule,
    ToastrModule.forRoot()
  ],
  providers: [CourierDetailService, PackageDetailService],
  bootstrap: [AppComponent]
})
export class AppModule { }
