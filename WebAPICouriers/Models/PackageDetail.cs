﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace WebAPICouriers.Models
{
    public class PackageDetail
    {
        [Key]
        public int PackageId { get; set; }

        [Required]
        [Column(TypeName = "nvarchar(50)")]
        public string Sender { get; set; }

        [Required]
        [Column(TypeName = "nvarchar(50)")]
        public string Recipient { get; set; }

        [Required]
        [Column(TypeName = "varchar(10)")]
        public string SendingDate { get; set; }

        [Required]
        [Column(TypeName = "nvarchar(50)")]
        public int Courier { get; set; }
    }
}
