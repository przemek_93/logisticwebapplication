﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace WebAPICouriers.Models
{
    public class CourierDetail
    {
        [Key]
        public int CourierId { get; set; }

        [Required]
        [Column(TypeName ="nvarchar(30)")]
        public string FirstName { get; set; }

        [Required]
        [Column(TypeName = "nvarchar(30)")]
        public string LastName { get; set; }

        [Required]
        [Column(TypeName = "varchar(12)")]
        public int PhoneNumber { get; set; }

    }
}
