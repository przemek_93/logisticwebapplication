﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebAPICouriers.Models;

namespace WebAPICouriers.Models
{
    public class CourierDetailContext:DbContext
    {
        public CourierDetailContext(DbContextOptions<CourierDetailContext> options):base(options)
        {
                
        }

        public DbSet<CourierDetail> CourierDetails { get; set; }

        public DbSet<WebAPICouriers.Models.PackageDetail> PackageDetails { get; set; }
    }
}
