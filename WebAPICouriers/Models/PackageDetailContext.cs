﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebAPICouriers.Models
{
    public class PackageDetailContext:DbContext
    {
        public PackageDetailContext(DbContextOptions<PackageDetailContext> options):base(options)
        {

        }

        public DbSet<PackageDetail> PackageDetails { get; set; }

        public DbSet<WebAPICouriers.Models.CourierDetail> CourierDetails { get; set; }

    }
}
