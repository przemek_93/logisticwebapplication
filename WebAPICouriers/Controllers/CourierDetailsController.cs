﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using WebAPICouriers.Models;

namespace WebAPICouriers.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CourierDetailsController : ControllerBase
    {
        private readonly CourierDetailContext _context;

        public CourierDetailsController(CourierDetailContext context)
        {
            _context = context;
        }

        // GET: api/CourierDetails
        [HttpGet]
        public async Task<ActionResult<IEnumerable<CourierDetail>>> GetCourierDetails()
        {
            return await _context.CourierDetails.ToListAsync();
        }

        // PUT: api/CourierDetails/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutCourierDetail(int id, CourierDetail courierDetail)
        {
            if (id != courierDetail.CourierId)
            {
                return BadRequest();
            }

            _context.Entry(courierDetail).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!CourierDetailExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // GET: api/CourierDetails/5
        [HttpGet("{id}")]
        public async Task<ActionResult<CourierDetail>> GetCourierDetail(int id)
        {
            var courierDetail = await _context.CourierDetails.FindAsync(id);

            if (courierDetail == null)
            {
                return NotFound();
            }

            return courierDetail;
        }

        // POST: api/CourierDetails
        [HttpPost]
        public async Task<ActionResult<CourierDetail>> PostCourierDetail(CourierDetail courierDetail)
        {
            _context.CourierDetails.Add(courierDetail);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetCourierDetail", new { id = courierDetail.CourierId }, courierDetail);
        }

        // DELETE: api/CourierDetails/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<CourierDetail>> DeleteCourierDetail(int id)
        {
            var courierDetail = await _context.CourierDetails.FindAsync(id);
            if (courierDetail == null)
            {
                return NotFound();
            }

            _context.CourierDetails.Remove(courierDetail);
            await _context.SaveChangesAsync();

            return courierDetail;
        }

        private bool CourierDetailExists(int id)
        {
            return _context.CourierDetails.Any(e => e.CourierId == id);
        }
    }
}
