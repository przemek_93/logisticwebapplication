﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using WebAPICouriers.Models;

namespace WebAPICouriers.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PackageDetailsController : ControllerBase
    {
        private readonly PackageDetailContext _context;

        public PackageDetailsController(PackageDetailContext context)
        {
            _context = context;
        }

        // GET: api/PackageDetails
        [HttpGet]
        public async Task<ActionResult<IEnumerable<PackageDetail>>> GetPackageDetails()
        {
            return await _context.PackageDetails.ToListAsync();
        }

        // GET: api/PackageDetails/5
        [HttpGet("{id}")]
        public async Task<ActionResult<PackageDetail>> GetPackageDetail(int id)
        {
            var packageDetail = await _context.PackageDetails.FindAsync(id);

            if (packageDetail == null)
            {
                return NotFound();
            }

            return packageDetail;
        }

        // PUT: api/PackageDetails/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutPackageDetail(int id, PackageDetail packageDetail)
        {
            if (id != packageDetail.PackageId)
            {
                return BadRequest();
            }

            _context.Entry(packageDetail).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!PackageDetailExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/PackageDetails
        [HttpPost]
        public async Task<ActionResult<PackageDetail>> PostPackageDetail(PackageDetail packageDetail)
        {
            _context.PackageDetails.Add(packageDetail);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetPackageDetail", new { id = packageDetail.PackageId }, packageDetail);
        }

        // DELETE: api/PackageDetails/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<PackageDetail>> DeletePackageDetail(int id)
        {
            var packageDetail = await _context.PackageDetails.FindAsync(id);
            if (packageDetail == null)
            {
                return NotFound();
            }

            _context.PackageDetails.Remove(packageDetail);
            await _context.SaveChangesAsync();

            return packageDetail;
        }

        private bool PackageDetailExists(int id)
        {
            return _context.PackageDetails.Any(e => e.PackageId == id);
        }
    }
}
